# Use the official OpenJDK 17 image as the base
FROM openjdk:17-jdk-alpine

# Set the working directory inside the container
WORKDIR /app

# Install findutils for xargs
RUN apk add --no-cache findutils

# Copy the Gradle wrapper and build scripts first (to leverage caching)
COPY gradlew .
COPY gradle ./gradle

# Grant permission to execute the Gradle wrapper
RUN chmod +x gradlew

# Copy the project files into the container
COPY . .

# Build the Spring Boot application
RUN ./gradlew clean bootJar

# Expose the port on which the Spring Boot application runs
EXPOSE 8080

# Set the entry point to run the built JAR file
ENTRYPOINT ["java", "-jar", "build/libs/SpringVueProject-0.0.1.jar"]
