package content.management.SpringVueProject.Controller;

import content.management.SpringVueProject.Object.User;
import content.management.SpringVueProject.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping(value = "/")
    public ResponseEntity<List<User>> getAll(){
        List<User> userList = userService.getAll();
        return new ResponseEntity<>(userList,HttpStatus.OK);
    }
    @GetMapping(value = "/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id){
        User user =userService.getUser(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<User> SaveUser(@RequestBody User user){
        User saveUser =userService.saveUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }
}
