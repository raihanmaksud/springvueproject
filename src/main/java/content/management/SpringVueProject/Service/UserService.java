package content.management.SpringVueProject.Service;

import content.management.SpringVueProject.Object.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService {

    List<User> userList = new ArrayList<>(
            Arrays.asList(
                new User(1L,"raymond","maxwell","rmaxwell@gmail.com"),
                new User(2L,"Tasser","alexandra","talexandra@gmail.com"),
                new User(3L,"picco","mister","pmister@gmail.com"),
                new User(4L,"simon","jewella","sjewella@gmail.com"),
                new User (5L,"tania","roxy","troxy@gmail.com")
            )
    );
    public List<User> getAll(){
        return userList;
    }
    public User getUser(Long id){
        return userList.stream().filter(user -> user.getId().equals(id)).findAny().orElse(null);
    }

    public User saveUser(User user){
        userList.add(user);
        return user;
    }
    public int addition(final int num1 , final int num2){
        return num1+num2;
    }
}
