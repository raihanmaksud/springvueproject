package content.management.SpringVueProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringVueProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringVueProjectApplication.class, args);
	}

}
