package content.management.SpringVueProject.Service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    UserService service;

    @Test
    public void addition(){
        final int a =1;
        final int b = 2;
        int result = service.addition(a,b);
        assertEquals(3,result);

    }
}